package io;

import dictionary.Dictionary;

public interface IO {
	
	public boolean SaveTXT(Dictionary dictionary);
	
	public boolean LoadTXT();

}
