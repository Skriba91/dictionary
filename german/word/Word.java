package word;

public interface Word {
	
	/**
	 * Getter for word type.
	 * @return Returns a string that contains the type of the word.
	 */
	public String getType();
	
	/**
	 * Getter for article.
	 * @return Returns a string that contains the article of the word.
	 */
	public String getArticle();
	
	/**
	 * Getter for the word.
	 * @return Returns a string with the word.
	 */
	public String getWord();
	
	/**
	 * Getter for the plural form of the word.
	 * @return Returns a string with the plural form of the word.
	 */
	public String getPlural();

}
