package word;

public enum Type {
	
	NOUN("noun"),
	VERB("verb"),
	ADJECTIVE("adjective"),
	ADVERB("adverb"),
	OTHER("other");
	
	public String type;
	
	Type(String type) {
		this.type = type;
	}

}
