package word;


public class German implements Word {
	
	Type type;
	Article article;
	String word;
	String plural;
	
	German(String word) {
		this.word = word;
		type = Type.OTHER;
		word = "";
		plural = "";
	}

	German(String word, Type type) {
		this.word = word;
		this.type = type;
		word = "";
		plural = "";
	}
	
	German(String word, Type type, Article article, String plural) {
		this.word = word;
		this.type = type;
		this.article = article;
		this.plural = plural;
	}
	
	@Override
	public String toString() {
		String value = type + ";" + article + ";" + word + ";" + plural;
		return value;
	}

	@Override
	public String getType() {
		return type.type;
	}

	@Override
	public String getArticle() {
		return article.article;
	}

	@Override
	public String getWord() {
		return word;
	}

	@Override
	public String getPlural() {
		return plural;
	}

}
