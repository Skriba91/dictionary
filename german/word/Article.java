package word;

public enum Article {
	DER("der"),
	DIE("die"),
	DAS("das");
	
	public String article;
	
	private Article(String article) {
		this.article = article;
	}

}
